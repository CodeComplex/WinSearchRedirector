Restores back functionality, recently removed by Microsoft, that allowed for 
Cortana searches to be opened in the default web browser registered on the 
system; adds Google & Duck Duck Go search providers, in addition to Bing. Also, 
allows to choose between various browsers (Chrome, IE, Firefox & Opera).
FOSS. All managed code - C#.