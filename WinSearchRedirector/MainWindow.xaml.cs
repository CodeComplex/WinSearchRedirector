﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using Condition = System.Windows.Automation.Condition;

namespace WinSearchRedirector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private ComboBox _cboSearchProviders;
        private ComboBox _cboWebBrowsers;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            
            SourceInitialized += MainWindow_SourceInitialized;
        }

        /// <summary>
        /// Handles the SourceInitialized event of the MainWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            SourceInitialized -= MainWindow_SourceInitialized;

            TrayIcon.ContextMenu = CreateTrayMenu();

            WinHook.WindowShown += WinHook_WindowShown;
            if (!WinHook.SetHook())
            {
                WinHook.WindowShown -= WinHook_WindowShown;
                MessageBox.Show("Unable to hook new window events on this system.");
            }
        }

        /// <summary>
        /// Creates the context menu for use by the tray icon.
        /// </summary>
        /// <returns>ContextMenu.</returns>
        private ContextMenu CreateTrayMenu()
        {
            var ver = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            var ctx = new ContextMenu();
            var item = new MenuItem { Header = $"WinSearch Redirector v. {ver}", IsEnabled = false };
            ctx.Items.Add(item);

            ctx.Items.Add(new Separator());

            //create web browsers combobox
            _cboWebBrowsers = new ComboBox {Width = 120};
            var browsers = GenerateBrowserItems();
            browsers.ForEach(b => _cboWebBrowsers.Items.Add(b));
            _cboWebBrowsers.SelectedIndex = 0;

            ctx.Items.Add(_cboWebBrowsers);

            //create search providers combobox
            _cboSearchProviders = new ComboBox {Width = 120};
            var providers = GenerateSearchProviders();
            providers.ForEach(p => _cboSearchProviders.Items.Add(p));
            _cboSearchProviders.SelectedIndex = 0;

            ctx.Items.Add(_cboSearchProviders);

            ctx.Items.Add(new Separator());

            item = new MenuItem { Header = "Exit" };
            item.Click += Item_Click;
            ctx.Items.Add(item);

            return ctx;
        }

        private static List<ComboBoxItem> GenerateBrowserItems()
        {
            return new List<ComboBoxItem>
            {
                new ComboBoxItem {Content = "Default Browser" }
                , new ComboBoxItem { Content = "Chrome", Tag = "chrome.exe"}
                , new ComboBoxItem {Content = "Internet Explorer", Tag = "iexplore.exe"}
                , new ComboBoxItem { Content = "Firefox", Tag = "firefox.exe"}
                , new ComboBoxItem {Content = "Opera", Tag = "opera.exe"}
            };
        } 

        /// <summary>
        /// Generates the search provider combobox items.
        /// </summary>
        /// <returns>List&lt;ComboBoxItem&gt;.</returns>
        private static List<ComboBoxItem> GenerateSearchProviders()
        {
            return new List<ComboBoxItem>
            {
                new ComboBoxItem {Content = "Bing"}
                , new ComboBoxItem {Content = "Google", Tag = "https://www.google.com/?gws_rd=ssl#q={0}" }
                , new ComboBoxItem {Content = "Duck Duck Go", Tag = "https://duckduckgo.com/?q={0}" }
            };
        }

        /// <summary>
        /// Handles the WindowShown event of the WinHook control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="HookEventArgs"/> instance containing the event data.</param>
        private void WinHook_WindowShown(object sender, HookEventArgs e)
        {
            if (e.ActiveHandle == IntPtr.Zero) return;

            //Obtain the URL from edge
            var url = GetEdgeUrl();

            //no url? bail out, here
            if (url == null)
                return;

            //Terminate Edge
            Win32.KillWindow(e.ActiveHandle);

            var provider = (ComboBoxItem)_cboSearchProviders.SelectedItem;
            if (!provider.Content.Equals("Default"))
            {
                var parms = HttpUtility.ParseQueryString(url.Query);
                var terms = parms["q"];
                var searchString = provider.Tag?.ToString();

                if (!string.IsNullOrWhiteSpace(searchString))
                {
                    searchString = string.Format(searchString, terms);
                    Uri.TryCreate(searchString, UriKind.Absolute, out url);
                }
            }

            var browser = string.Empty;
            if (_cboWebBrowsers.SelectedIndex > 0)
            {
                browser = ((ComboBoxItem) _cboWebBrowsers.SelectedItem).Tag.ToString();
            }

            using (var p = new Process())
            {
                var psi = new ProcessStartInfo();
                if (!string.IsNullOrWhiteSpace(browser))
                {
                    psi.FileName = browser;
                    psi.Arguments = url.AbsoluteUri;
                }
                else
                    //run the default browser
                    psi.FileName = url.AbsoluteUri;

                //Launch the obtained url with the default web browser
                p.StartInfo = psi;
                p.Start();
            }
        }

        /// <summary>
        /// Handles the Click event of the 'Exit' menu item control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Item_Click(object sender, RoutedEventArgs e)
        {
            //hide the tray icon
            TrayIcon.Visibility = Visibility.Collapsed;

            //remove event handler and unhook
            WinHook.WindowShown -= WinHook_WindowShown;
            WinHook.UnHook();

            Close();
        }

        /// <summary>
        /// Obtains and returns the URL that was passed to Microsoft Edge.
        /// </summary>
        /// <returns>Uri.</returns>
        private static Uri GetEdgeUrl()
        {
            var automationElements = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);

            try
            {
                foreach (var child in automationElements.Cast<AutomationElement>().Where(child => child.Current.ClassName.Equals("ApplicationFrameWindow")))
                {
                    var element = child.FindAll(TreeScope.Children, Condition.TrueCondition);
                    var edge = (element.Cast<AutomationElement>()).FirstOrDefault(
                        c => c.Current.Name.Equals("microsoft edge", StringComparison.InvariantCultureIgnoreCase));

                    var addressBox =
                        edge?.FindAll(TreeScope.Children, Condition.TrueCondition)
                            .Cast<AutomationElement>().FirstOrDefault(
                                c => c.Current.AutomationId.Equals("addressEditBox", StringComparison.InvariantCulture));

                    if (addressBox == null)
                        continue;

                    var url = ((TextPattern)addressBox.GetCurrentPattern(TextPattern.Pattern)).DocumentRange.GetText(int.MaxValue);

                    if (!url.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                        url = $"https://{url}";

                    Uri searchUrl;
                    if (!Uri.TryCreate(url, UriKind.Absolute, out searchUrl))
                        continue;

                    var parms = HttpUtility.ParseQueryString(searchUrl.Query);
                    var form = parms["form"];

                    //was Edge launched through Windows Search?
                    if (form.Equals("wnsgph", StringComparison.InvariantCultureIgnoreCase))
                        return searchUrl;
                }
            }
            catch (ElementNotAvailableException e)
            {
                Console.WriteLine(e);
            }

            return null;
        }
    }
}
