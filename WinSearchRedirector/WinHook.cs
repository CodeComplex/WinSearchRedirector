﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace WinSearchRedirector
{
    [System.Security.SuppressUnmanagedCodeSecurity]
    public static class WinHook
    {
        #region Windows API

        private enum SystemEvents : uint
        {
            EVENT_OBJECT_SHOW = 0x8002,  // hwnd + ID + idChild is shown item
        }


        private const uint WINEVENT_OUTOFCONTEXT = 0x0000;
        private const uint WINEVENT_SKIPOWNPROCESS = 0x0002;

        private delegate void WinEventDelegate(
            IntPtr hWinEventHook,
            uint eventType,
            IntPtr hWnd,
            int idObject,
            int idChild,
            uint dwEventThread,
            uint dwmsEventTime);

        [DllImport("User32.dll", SetLastError = true)]
        private static extern IntPtr SetWinEventHook(
            uint eventMin,
            uint eventMax,
            IntPtr hmodWinEventProc,
            WinEventDelegate lpfnWinEventProc,
            uint idProcess,
            uint idThread,
            uint dwFlags);

        [DllImport("user32.dll")]
        private static extern bool UnhookWinEvent(IntPtr hWinEventHook);

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is hooked.
        /// </summary>
        /// <value><c>true</c> if this instance is hooked; otherwise, <c>false</c>.</value>
        private static bool IsHooked { get; set; }
        #endregion

        private static IntPtr _ptrHook;
        private static WinEventDelegate _winEventHandler;

        /// <summary>
        /// Occurs when a window has been shown.
        /// </summary>
        public static event EventHandler<HookEventArgs> WindowShown;

        /// <summary>
        /// Sets a hook to watch for new windows being displayed.
        /// </summary>
        /// <returns><c>true</c> if the hook was successfully set, <c>false</c> otherwise.</returns>
        public static bool SetHook()
        {
            if (!IsHooked)
            {
                _winEventHandler = WinEventHandler;

                const uint flag = WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNPROCESS;

                _ptrHook = SetWinEventHook((uint)SystemEvents.EVENT_OBJECT_SHOW,
                                        (uint)SystemEvents.EVENT_OBJECT_SHOW,
                                        IntPtr.Zero, _winEventHandler, 0, 0, flag);
            }

            return (IsHooked = (_ptrHook != IntPtr.Zero));
        }

        private static void WinEventHandler(IntPtr hWinEventHook, uint eventType, IntPtr hWnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            if (hWinEventHook == _ptrHook && idObject == 0x00000000 && idChild == 0x00000000) // Check for window events
            {
                if (eventType == (uint)SystemEvents.EVENT_OBJECT_SHOW)
                {
                    var title = Win32.GetTitle(hWnd);
                    if (string.IsNullOrWhiteSpace(title)) return;

                    if (title.Equals("microsoft edge", StringComparison.InvariantCultureIgnoreCase))
                        OnWindowShown(new HookEventArgs(hWnd));
                }
            }
        }

        /// <summary>
        /// Removes the event hook.
        /// </summary>
        public static void UnHook()
        {
            if (IsHooked)
            {
                if (!IntPtr.Zero.Equals(_ptrHook))
                {
                    UnhookWinEvent(_ptrHook);
                    GC.KeepAlive(_winEventHandler);
                }

                IsHooked = false;
            }

            _ptrHook = IntPtr.Zero;
        }

        #region WinEventHook callback (wndproc substitution)

        #endregion

        private static void OnWindowShown(HookEventArgs e)
        {
            WindowShown?.Invoke(null, e);
        }
    }

    public class HookEventArgs : EventArgs
    {
        /// <summary>
        /// Gets the active window handle.
        /// </summary>
        /// <value>The active handle.</value>
        public IntPtr ActiveHandle { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="HookEventArgs"/> class.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        public HookEventArgs(IntPtr hwnd)
        {
            ActiveHandle = hwnd;
        }
    }

    internal static class Win32
    {

        [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Auto)] //
        private static extern bool SendMessage(IntPtr hWnd, uint msg, int wParam, StringBuilder lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);


        /// <summary>
        /// Gets the title of the window which is associated with the supplied handle.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <returns>System.String.</returns>
        internal static string GetTitle(IntPtr hwnd)
        {
            var sz = SendMessage(hwnd, 0x000E, IntPtr.Zero, IntPtr.Zero);
            if (sz != IntPtr.Zero)
            {
                var sb = new StringBuilder(4096);
                SendMessage(hwnd, 0x000D, sb.Capacity, sb);
                var text = sb.ToString();

                return text;
            }
            return null;
        }

        /// <summary>
        /// Closes the specified window.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        internal static void KillWindow(IntPtr hwnd)
        {
            SendMessage(hwnd, 0x0112, new IntPtr(0xF060), IntPtr.Zero);
        }

    }

    
}
